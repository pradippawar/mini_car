<?php

	if(!function_exists('check_digits'))
	{
		function check_digits($data,$range)
		{
			$pattern = "/^[\d]{".$range."}$/";
			return preg_match($pattern,$data);
		}
	}


	if(!function_exists('my_alpha_number'))
	{
		function my_alpha_number($data,$range)
		{
			$pattern = "/^[A-z0-9 ]{".$range."}$/";
			return preg_match($pattern,trim($data));
		}
	}

	if(!function_exists('my_alpha_space'))
	{
		function my_alpha_space($data,$range)
		{
			$pattern = "/^[A-z ]{".$range."}$/";
			return preg_match($pattern,trim($data));
		}
	}

	if(!function_exists('file_upload'))
	{
		function file_upload($folder,$size,$filedata,$type,$uniquepath)
		{
			if(empty($filedata['name'][0]))
			{
				return "Invalid Name";
			}
			
			$total_file_size = $filedata['size'];
			
			if($total_file_size>$size)
			{
				return "Invalid File Size";
			}
			
			$ans = in_array($filedata['type'],$type);
				
			if($ans!=1)
			{
				return "Invalid File Type";
			}
				
			$finalpath =$folder.$uniquepath.$filedata['name'];				
			$data[] = $finalpath;
			$temp = $filedata['tmp_name'];
			move_uploaded_file($temp,$finalpath);		
			
						
			return $data;
		}
	}

?>