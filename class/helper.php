<?php
require_once("connect.php");

abstract class helper extends connect{

	function insert($table,$data){
		$value="'".implode("','",$data)."'";

		$key=implode(",",array_keys($data));

		$str="insert into $table($key) values($value)";

		$result=$this->conn->query($str) or die($this->conn->error);

		if($result)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}


	function select($field,$table,$condition=1){

		$str="select $field from $table where $condition";

		$result=$this->conn->query($str) or die($this->conn->error);

		if($result->num_rows>0)
		{
			while($ans =  mysqli_fetch_array($result,MYSQLI_ASSOC))
			{
				$data[]=$ans;
			}

			return $data;
		}
		else
		{
			return 0;
		}
	}

	function sqlquery($query){

		$result=$this->conn->query($query) or die($this->conn->error);

		if($result->num_rows>0)
		{
			while($ans = mysqli_fetch_array($result,MYSQLI_ASSOC))
			{
				$data[]=$ans;
			}

			return $data;
		}
		else
		{
			return 0;
		}
	}

	function sqlquery_execute($query){

		$result=$this->conn->query($query) or die($this->conn->error);

	}

	function data_filter($data)
	{
		$ans = $this->conn->real_escape_string(strip_tags(trim($data)));
		
		return $ans;
	}

	function affected_rows()
	{
		$ans = mysqli_affected_rows($this->conn);
		
		return $ans;
	}

	function cnt($table,$condition=1)
	{
		if(empty($condition)){ $condition=1; }
		
		$ans = $this->select(
			"count(*) as count",$table,$condition
		);

		if(!empty($ans))
		{
			return $ans[0]['count'];
		}
		else
		{
			return 0;
		}
		
	}
}

?>