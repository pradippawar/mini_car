<?php 
require_once("class/project.php");

$success='0';
$model = my_alpha_number($_POST['model_name'],"3,50");
if($model==1)
{
	$color = my_alpha_space($_POST['color'],"3,20");
	if($color==1)
	{
		$year = check_digits($_POST['manufacturing_year'],"4");
		if($year==1)
		{
			$reg_num = my_alpha_number($_POST['registration_number'],"3,100");
			if($reg_num==1)
			{
				if($_FILES['image_one']['error']=='0')
				{
					$reg=$_POST['registration_number'];
					$cnt = $obj->cnt("car_models","registration_number='$reg'");
					if($cnt>0)
					{
						$msg = "Model With This Registration Number already Exist";
					}
					else
					{

						$unique = time();
							
						$image1 = file_upload(
							"upload/",
							2*1024*1024,
							$_FILES['image_one'],
							array("image/jpeg","image/png","image/gif"),
							$unique
						);
						

						if($_FILES['image_two']['error']=='0')
						{
							$image2 = file_upload(
								"upload/",
								2*1024*1024,
								$_FILES['image_two'],
								array("image/jpeg","image/png","image/gif"),
								$unique
							);
						}

						if(isset($image1)&&(!is_array($image1)))
						{
							echo $msg=$image1." In Image 1";
							exit;
						}
						elseif(isset($image1)&&is_array($image1))
						{
							$_POST['image_one']=$image1[0];
						}


						if(isset($image2)&&(!is_array($image2)))
						{
							echo $msg=$image2." In Image 2";
							exit;
						}
						elseif(isset($image2)&&is_array($image2))
						{
							$_POST['image_two']=$image2[0];
						}

						$obj->insert("car_models",$_POST);
				
						$msg = "Model added Successfully";
						$success='1';
						//header.location()
					}
				}
				else
				{
					$msg="Upload Atleast One Image";
				}
			}
			else
			{
				$msg="Enter Valid Registration Number";
			}
		}
		else
		{
			$msg="Enter Valid Year";
		}
	}
	else
	{
		$msg="Enter Valid Color";
	}
}
else
{
	$msg="Enter Valid Model";
}

echo "###".$success."###".$msg;
header("refresh:2;url=index.php");

?>