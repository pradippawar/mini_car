-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2019 at 05:04 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `code`
--

-- --------------------------------------------------------

--
-- Table structure for table `car_manufacturers`
--

CREATE TABLE `car_manufacturers` (
  `id` int(11) UNSIGNED NOT NULL,
  `manufacturer_name` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_manufacturers`
--

INSERT INTO `car_manufacturers` (`id`, `manufacturer_name`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Maruti', 1, 0, '2019-04-15 13:50:52', '2019-04-15 13:50:52'),
(2, 'Tata', 1, 0, '2019-04-15 13:50:52', '2019-04-15 13:50:52'),
(3, 'BMW', 1, 0, '2019-04-16 14:25:08', NULL),
(4, 'Honda', 1, 0, '2019-04-16 19:53:59', NULL),
(5, 'Toyoto', 1, 0, '2019-04-16 19:56:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_models`
--

CREATE TABLE `car_models` (
  `id` int(11) UNSIGNED NOT NULL,
  `manufacturer_id` int(11) UNSIGNED NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  `manufacturing_year` varchar(50) NOT NULL,
  `registration_number` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `image_one` text NOT NULL,
  `image_two` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_models`
--

INSERT INTO `car_models` (`id`, `manufacturer_id`, `model_name`, `color`, `manufacturing_year`, `registration_number`, `note`, `image_one`, `image_two`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'WagonR', 'Black', '2019', 'reg123', 'HatchBack', '', '', 1, 0, '2019-04-15 14:36:03', NULL),
(2, 2, 'Indigo', 'White', '2019', 'reg123', 'Sedan', '', '', 1, 0, '2019-04-15 14:36:35', NULL),
(3, 2, 'Indigo', 'Black', '2019', 'reg123', 'Sedan', '', '', 1, 0, '2019-04-15 14:36:35', NULL),
(4, 3, '720s', 'Black', '2019', 'reg4321', 'Data', 'upload/1555423211pexels-photo-248797.jpeg', '', 1, 0, '2019-04-16 19:30:11', NULL),
(6, 4, 'Civic', 'Blue', '2019', 'civic123', 'Honda', 'upload/155542529661hi9EPP7UL._UY560_.jpg', 'upload/1555425296cc_2018hoc020028_02_640_bs.jpg', 1, 0, '2019-04-16 20:04:56', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car_manufacturers`
--
ALTER TABLE `car_manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_models`
--
ALTER TABLE `car_models`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer_id_foreign` (`manufacturer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car_manufacturers`
--
ALTER TABLE `car_manufacturers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `car_models`
--
ALTER TABLE `car_models`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `car_models`
--
ALTER TABLE `car_models`
  ADD CONSTRAINT `manufacturer_id_foreign` FOREIGN KEY (`manufacturer_id`) REFERENCES `car_manufacturers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
