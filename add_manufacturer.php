<?php 
require_once("class/project.php");
require_once("header.php");

$result=$obj->get_manufacturer();

?>
  
<div class="container">
  <h2>Manufacturers</h2>
  <p>Add New Manufacturers</p>            
  <div class="row">
    <div class="col-sm-8">
      <form id='manufacturer_form'>
        <div class="col-sm-3">
          <label style="width: 100%;text-align: center;">Manufacturer Name</label>
        </div>
         <div class="col-sm-6" >
          <input type="text" name="manufacturer_name" placeholder="Manufacturer Name" style="width: 100%;" >
        </div>
        <div class="col-sm-3">
          <button id='btn_submit' type='button' style="width: 100%;">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="container">
  <h3>Manufacturers List</h3>           
  <table id="example" class="table table-bordered">
    <thead>
      <tr>
        <th>Serial Number</th>
        <th>Manufacturer Name</th>
      </tr>
    </thead>
    <tbody>
    <?php 
    if(!empty($result)):
      $i=1;
      foreach($result as $key=>$val): ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $val['manufacturer_name']; ?></td>
      </tr>
    <?php 
      endforeach; 
    endif;
    ?>
      
    </tbody>
  </table>
</div>


<?php require_once("footer.php") ?>