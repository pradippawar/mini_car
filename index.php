<?php 
require_once("class/project.php");
require_once("header.php");

$str="select cmf.manufacturer_name, cml.model_name,count(cml.id) as count,cml.id,GROUP_CONCAT(cml.color) as color,cml.manufacturing_year,cml.registration_number,cml.note,cml.image_one,cml.image_two
from car_manufacturers cmf
join car_models cml on cml.manufacturer_id=cmf.id
where cml.is_active='1' and cml.is_deleted='0'
group by cml.model_name
order by cmf.manufacturer_name,cml.model_name
" ;
$result=$obj->sqlquery($str);

?>
  
<div class="container">
  <h2>Models List</h2>           
  <table id="example" class="table table-bordered">
    <thead>
      <tr>
        <th>Serial Number</th>
        <th>Manufacturer Name</th>
        <th>Model Name</th>
        <th>Count</th>
      </tr>
    </thead>
    <tbody>
    <?php 
    if(!empty($result)):
      $i=1;
      foreach($result as $key=>$val): ?>
      <tr onclick='showdetails("<?= $val['id']?>","<?= $val['model_name']?>","<?= $val['manufacturer_name']?>","<?= $val['color']?>","<?= $val['manufacturing_year']?>","<?= $val['registration_number']?>","<?= $val['note']?>","<?= $val['image_one']?>","<?= $val['image_two']?>")'>
        <td><?= $i++; ?></td>
        <td><?= $val['manufacturer_name']; ?></td>
        <td><?= $val['model_name']; ?></td>
        <td><?= $val['count']; ?></td>
      </tr>
    <?php 
      endforeach; 
    endif;
    ?>
      
    </tbody>
  </table>
</div>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Car Details</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="col-sm-4">
                <input type="hidden" id="model_id"  name="model_id">
                <label  style="width: 100%;text-align: center;">Model Name:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" id="model_name" placeholder="Model Name" name="model_name" style="width: 100%;text-align: center;">
              </div><br><br>
              <div class="col-sm-4">
                <label  style="width: 100%;text-align: center;">Manufacturer Name:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" id="manufacturer_name" placeholder="Manufacturer Name" name="manufacturer_name" style="width: 100%;text-align: center;">
              </div><br><br>
              <div class="col-sm-4">
                <label  style="width: 100%;text-align: center;">Color:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" id="color" placeholder="Color" name="color" style="width: 100%;text-align: center;">
              </div><br><br>
              <div class="col-sm-4">
                <label  style="width: 100%;text-align: center;">Manufacturing Year:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" id="manufacturing_year" placeholder="Manufacturing Year" name="manufacturing_year" style="width: 100%;text-align: center;">
              </div><br><br>
              <div class="col-sm-4">
                <label  style="width: 100%;text-align: center;">Registration No:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" id="registration_no" placeholder="Registration No" name="registration_no" style="width: 100%;text-align: center;">
              </div><br><br>
              <div class="col-sm-4">
                <label  style="width: 100%;text-align: center;">Note :</label>
              </div>
              <div class="col-sm-8">
                <input type="text" id="note" placeholder="Note" name="note" style="width: 100%;text-align: center;">
              </div><br><br>
              <div class="col-sm-6" align="center">
                <label>Image 1</label><br>
               <img id="image_one" src="" alt="No Image" width="100%" height="100%" > 
              </div>
              <div class="col-sm-6" align="center">
                <label>Image 2</label><br>
               <img id="image_two" src="" alt="No Image" width="100%" height="100%"> 
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick='update_models()'>Sold</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


<?php require_once("footer.php") ?>