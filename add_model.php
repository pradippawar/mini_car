<?php 
require_once("class/project.php");
require_once("header.php");

$mf=$obj->get_manufacturer();
if(empty($mf))
{
  $mf=array();
}
?>
  
<div class="container">
  <h2>Models</h2>
  <p>Add New Models</p>
  <form class="form-horizontal" id="model_form"  name="model_form" method="post" enctype="multipart/form-data" accept-charset="utf-8">
    <div class="form-group">
      <label class="control-label col-sm-2" >Model Name:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="model_name" placeholder="Model Name" name="model_name">
      </div>
      <label class="control-label col-sm-2" >Manufacturer Name:</label>
      <div class="col-sm-4">
        <select class="form-control" id="manufacturer_id" placeholder="Manufacturer Name" name="manufacturer_id">
          <?php foreach ($mf as $value) { ?>
            <option value="<?= $value['id'] ?>"><?= $value['manufacturer_name'] ?></option>
          <?php } ?>
        </select>
        
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" >Color:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="color" placeholder="Enter Color" name="color">
      </div>
      <label class="control-label col-sm-2" >Manufactring Year:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="manufacturing_year" placeholder="Manufactring Year" name="manufacturing_year">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" >Registration No:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="registration_number" placeholder="Registration No" name="registration_number">
      </div>
      <label class="control-label col-sm-2" >Note:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="note" placeholder="Note" name="note">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" >Image:</label>
      <div class="col-sm-4">
        <input type="file" class="form-control" id="image_one" placeholder="Select Image" name="image_one">
      </div>
      <label class="control-label col-sm-2" >Image:</label>
      <div class="col-sm-4">
        <input type="file" class="form-control" id="image_two" placeholder="Select Image" name="image_two">
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default" id='btn_submit'>Submit</button>
      </div>
    </div>
  </form>
</div>


<?php require_once("footer.php") ?>