<?php $page_title= basename($_SERVER['PHP_SELF'],".php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Car Inventory System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="custom_script/bootbox.min.js"></script>


  <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Car Inventory System</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="<?php echo $page_title=='index' ? 'active' : ''; ?>"><a href="index.php">Home</a></li>
      <li class="<?php echo $page_title=='add_manufacturer' ? 'active' : ''; ?>"><a href="add_manufacturer.php">Add Manufacturer</a></li>
      <li class="<?php echo $page_title=='add_model' ? 'active' : ''; ?>"><a href="add_model.php">Add Model</a></li>
    </ul>
  </div>
</nav>

