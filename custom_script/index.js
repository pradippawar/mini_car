$(document).ready(function() {
    $('#example').DataTable();
} );

function showdetails(id,model,manufacturer,color,year,reg,note,image_one,image_two){

	jQuery.noConflict(); 
	$("#model_id").val(id);
	$("#model_name").val(model);
	$("#manufacturer_name").val(manufacturer);
	$("#color").val(color);
	$("#manufacturing_year").val(year);
	$("#registration_no").val(reg);
	$("#note").val(note);
	$('#image_one').attr('src',image_one);
	$('#image_two').attr('src',image_two);
	$('#myModal').modal('toggle');
}

function update_models(){
	var mname=$("#model_name").val();
	//alert(mname);
	$.ajax({
		url:'ajax_update_models.php',
		type:'post',
		data:{'model':mname},
		success:function(response){
			var res=response.split('###')
			if(res[1]==1)
			{
				alert('Model Updated')
				window.location.href = "index.php";
			}
		}
	})
}